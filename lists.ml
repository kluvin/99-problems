open Base

let rec last = function
  | []   -> None
  | [x]  -> Some x
  | _ :: tl -> last tl

let rec last_two = function
  | [] -> None
  | [a ; b] -> Some (a, b)
  | _ :: tl -> last_two tl

let rec at k = function
  | [] -> None
  | hd :: tl -> if k = 1 then Some hd else at (k - 1) tl
  
let at_tc k l =
  let rec f i = function
    | [] -> None
    | hd :: tl -> if k <> i
        then f (i + 1) tl
        else Some hd
in f 0 l

let length l =
  let rec aux i = function
    | [] -> i
    | _ :: tl -> aux (i + 1) tl
  in aux 0 l

let reverse l =
  let rec aux acc = function
    | [] -> acc
    | hd :: tl -> aux (hd :: acc) tl
  in aux [] l

let reverse2 l = List.fold l ~init:[] ~f:(fun acc a -> a :: acc)

type 'a nestedlist =
  | Leaf of 'a 
  | Tree of 'a nestedlist list

(* Using fold *)
let flatten l = 
  let rec f acc = function
    | Leaf x -> x :: acc
    | Tree x -> List.fold x ~init:acc ~f
  in List.fold l ~init:[] ~f
     |> List.rev


(* Not using fold *)
let flatten2 l =
  let rec f acc = function
    | [] -> acc
    | Leaf x :: tl -> f (x :: acc) tl
    | Tree x :: tl -> f (f acc x) tl in
  List.rev (f [] l)

let rec destutter = function
  | hd :: (hd' :: _ as tl) -> if hd = hd'
    then destutter tl
    else hd :: destutter tl
  | [] | [_] as rest -> rest

let pack l =
  let rec f groups currentgroup = function
    | [] -> []
    | [x] -> (x :: currentgroup) :: groups
    | hd :: (hd' :: _ as tl) -> if hd = hd'
      then f groups (hd :: currentgroup) tl
      else f ((hd :: currentgroup) :: groups) [] tl
  in f [] [] l
     |> List.rev

let encode l =
  let rec f count acc = function
    | [] -> []
    | [x] -> (count + 1, x) :: acc
    | hd :: (hd' :: _ as tl) -> if hd = hd'
      then f (count + 1) acc tl
      else f 0 ((count + 1, hd) :: acc) tl
  in f 0 [] l
     |> List.rev

(* The alternative implementation with pack is interesting.
   Included here for reference.
   Essentially, what is done is to tuple every element,
   and run-length encoding works with pack very nicely. *)
let encodewithpack list =
  List.map ~f:(fun l -> (List.length l, List.hd l)) (pack list)

type 'a rle =
  | One of 'a
  | Many of int * 'a

let modifiedencode l =
  let tupleof = function
    | (1, v) -> One v
    | (n, v) -> Many (n, v)
  in
  let rec f acc = function
    | [] -> []
    | [_ as hd]      ->    tupleof hd :: acc
    |  _ as hd :: tl -> f (tupleof hd :: acc) tl
  in f [] (encode l)
      |> List.rev

let decode l =
  let f acc = function
    | One v       -> String.make 1 (Char.of_string v) ^ acc
    | Many (n, v) -> String.make n (Char.of_string v) ^ acc
  in List.fold ~init:"" ~f l
     |> String.rev

(* Skipping directmodifiedencode because I can't see
   at first glance how it is different from modifiedencode *)

let duplicate l =
  let f acc x =  x :: x :: acc
  in List.fold ~f ~init:[] l
     |> List.rev

let duplicate_n n l =
  let repeat x =
    String.make n (Char.of_string x)
  in
  let rec f acc = function
    | [] -> []
    | [x] -> repeat x :: acc
    | hd :: tl -> f (repeat hd :: acc) tl
  in f [] l
     |> List.rev

(* Partially applied definition for duplicate (2) *)
let duplicate_papp = duplicate_n 2

let drop n l =
  let rec f acc c = function
    | [] -> acc
    | hd :: tl -> if c = 1
      then f acc n tl
      else f (hd :: acc) (c - 1) tl
  in f [] n l
     |> List.rev

let split n l =
  let rec f acc c = function
    | hd :: tl when c <> 0 -> f (hd :: acc) (c - 1) tl
    | _ as rest -> List.rev acc, rest
  in f [] n l

let slice l low high =
  let keep c = c >= low && c <= high in
  let rec f acc i = function
    | hd :: tl -> if keep i
      then f (hd :: acc) (i + 1) tl
      else f acc         (i + 1) tl 
    | _ -> acc
  in f [] 0 l
     |> List.rev

let rotate n l =
  let swap (a, b) = (b, a) in
  let rotation = function
    | n when n > 0 -> n
    | n -> List.length l + n in
  match swap (split (rotation n) l) with
    | (a, b) -> a @ b

let remove_at k l =
  let rec f acc i = function
    | hd :: tl -> if i = k
      then f acc (i + 1) tl
      else f (hd :: acc) (i + 1) tl
    | rest -> rest @ acc
  in f [] 0 l
  |> List.rev

let insert_at el k l =
  let rec f acc i = function
    | hd :: tl -> if i = k
      then f (hd :: el :: acc) (i + 1) tl
      else f (hd :: acc) (i + 1) tl
    | rest -> rest @ acc
  in f [] 0 l
  |> List.rev

(* Observing that insert_at and remove_at have a very common skeleton,
   which looks like it is trying to define it's own conditional fold. *)
let modify_at k consequent alternate l =
  let rec f i acc = function
    | hd :: tl -> if i = k
        then f (i + 1) (consequent hd acc) tl
        else f (i + 1) (alternate hd acc) tl
    | rest -> rest @ acc
  in f 0 [] l
  |> List.rev

let generic_modify_at k consequent alternate condition l =
  let rec f i acc = function
  | hd :: tl -> if (condition i k)
    then f (i + 1) (consequent hd acc) tl
    else  f (i + 1) (alternate hd acc) tl
  | rest -> rest @ acc
  in f 0 [] l
  |> List.rev

let insert_at2 el k l =
  let consequent hd acc = (hd :: el :: acc) in
  let alternate  hd acc = (hd :: acc) in 
    modify_at k consequent alternate l

(* Brilliant *)
let insert_at3 el k l =
  let f i acc a = if i = k then (a :: el :: acc) else (a :: acc)
  in List.foldi ~init:[] ~f l

let remove_at3  k l =
  let f i acc a = if i = k then acc else (a :: acc)
  in List.foldi ~init:[] ~f l

let remove_at2 k l =
  let consequent _ acc = acc in
  let alternate  hd acc = (hd :: acc) in
    modify_at k consequent alternate l
    
(* Not tail-recursive but a neat of solving the problem *)
let rec range i k =
  i ::
    if i = k
      then []
      else range (i + 1) k

open Random

(* rand-select seems a bit tricky to get right at first. There's a few time complexity traps,
   and maybe there is a problem with guaranteeing uniform distribution. *)

(* Also not tail-recursive *)
let rec randrange c n m =
  (Random.int_incl n m) :: if c = 0 then [] else randrange (c - 1) n m


let rec combinations n l = 
  match n, l with
  | (0 , _ ) -> [[]] (* Left.  All trees terminate with 0  *)
  | (_ , []) -> []   (* Right. All trees terminate with [] *)
  | (n , (hd :: tl)) ->
    let keep_hd l = (List.map ~f:(List.cons hd) l) in
    List.append
      (keep_hd (combinations (n-1) tl))
               (combinations  n    tl)
 
(* This is quite reminiscient of SICP's tags: With the maps
   we are only applying a tag to sort, then removing it. *)
let length_sort l =
  let compare a b =
    match fst a , fst b with
    | a , b when a > b -> 1
    | a , b when a < b -> (-2)
    | _ , _ -> 0
  in List.map l ~f:(fun a -> (List.length a , a))
  |> List.sort ~compare
  |> List.map ~f:snd

(* It seems like returning a type (Trues * Falses) or something similar
   would greatly improve the readability plus the type-checking.*)
let partition_tf l ~key =
  let rec aux ~t ~f = function
    | [] -> (t , f)
    | hd :: tl -> if (key hd)
      then aux ~t:(hd :: t) ~f tl
      else aux ~f:(hd :: f) ~t tl
  in aux ~t:[] ~f:[] l

(* Once you have a pair,
   you can have as many as you like.*)
let partition l =
  let rec aux acc = function
    | [] -> acc
    | hd :: tl -> let (eqs , neqs) =
      partition_tf tl ~key:(( = ) hd)
      in aux ((hd :: eqs) :: acc) neqs
  in aux [] l